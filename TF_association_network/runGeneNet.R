##===============================================================================
#
#  SCRIPT:    	runGeneNet.R
#
#  DESCRIPTION: Script to generate Gaussian Graphical Models for a range of 
#				fdr threhold values. The script takes in a multi-sample TF
#				binding profile as input and returns a list of undirected edges 
#				for each regularization parameter. Each output text file contains 
#				3 columns (node1, interaction, node2) and one row corresponds to 
#				one edge. The script also produces a density plot of partial 
#				correlation values and a graph of total number of edges discovered 
#				at different fdr threshold values.
#
#  AUTHOR:      Felicia Ng (fn231@cam.ac.uk)
#  INST: 	  	Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     22 Oct 2015
#
##==============================================================================

library(GeneNet)
library(ggplot2)
source("networkFunctions.R")

# Read input data
filename <- "TF_binding_profile.txt"
dat <- read.table(filename, sep="\t", header=T, check.names=F)
inputmat <- dat[,4:ncol(dat)] # Remove chr, start, end columns
lbl <- sort(colnames(inputmat))

# List of fdr threshold values
fdrlist <- format(1/10^seq(6,12), scientific=T)

# Estimate partial correlation matrix (static, with shrinkage)
inferred.pcor <- ggm.estimate.pcor(inputmat)

# Assign p-values, q-values, empirical posterior probabilities to all potential edges in the network
edges <- network.test.edges(inferred.pcor, plot=F)
n1 <- colnames(inputmat)[edges[,"node1"]]
n2 <- colnames(inputmat)[edges[,"node2"]]
edges[,"node1"] <- n1
edges[,"node2"] <- n2

# Decide which edges to include in the network (1 - local fdr)
for (i in 1:length(fdrlist)) {
	
	cat(paste("Running GeneNet at FDR ", fdrlist[i], "...\n", sep=""))
	
	thrlbl <- paste("fdr", fdrlist[i], sep="")
	
	thr <- 1 - as.numeric(fdrlist[i])
	net <- extract.network(edges, cutoff.ggm=thr, cutoff.dir=thr)
	edges.sig <- net[,c("node1", "node2")]
	
	# Unique edges
	edges.mat <- getUniqueEdges(edgesmat=edges.sig, labels=lbl)
	pcormat <- inferred.pcor*edges.mat
	
	# Get edges 
	edges.idx <- which(edges.mat>=1, arr.ind=T)
	node1 <- rownames(edges.mat)[edges.idx[,1]]
	node2 <- colnames(edges.mat)[edges.idx[,2]]
	edges.names <- data.frame(node1=node1, interaction=rep("pp", nrow(edges.idx)), node2=node2)
	
	# Save data
	write.table(edges.names, file=paste("edges_", as.character(thrlbl), ".txt", sep=""), sep="\t", row.names=F, col.names=F, quote=F)
	outfile <- paste("genenet_", as.character(thrlbl), ".RData", sep="")
	save(pcormat, edges.mat, edges.names, file=outfile)
	
	# Gather data for plotting (pcor values)
	val <- pcormat[pcormat!=0 & !is.na(pcormat)]
	mat <- cbind(val, rep(thrlbl, length(val)))
	if(exists("plotmat")) {
		plotmat <- rbind(plotmat, mat)
	}else {
		plotmat <- mat
	}
	
	# Gather data for plotting (total edges)
	tmp <- c(thrlbl, nrow(edges.names))
	if(exists("ct")) {
		ct <- rbind(ct, tmp)
	}else {
		ct <- tmp
	}
	
}

# Plot density of partial correlation values
plotmat2 <- data.frame(pcor=as.numeric(plotmat[,1]), fdr=plotmat[,2])
pdf("genenet_pcor_density.pdf")
a <- ggplot(plotmat2, aes(x=pcor, color=factor(fdr))) + geom_density()
print(a)
dev.off()

# Plot total number of edges
ct2 <- data.frame(fdr=as.character(ct[,1]), num_edges=as.numeric(ct[,2]))
write.table(ct2, file="genenet_edges_total_count.txt", sep="\t", row.names=F, col.names=T, quote=F)
pdf("genenet_edges_total_count.pdf")
b <- ggplot(ct2, aes(x=fdr, y=num_edges)) + geom_line(aes(group=1)) + theme(axis.text.x = element_text(angle = 90, hjust = 1))
print(b)
dev.off()