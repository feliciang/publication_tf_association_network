%%===============================================================================
%
%  SCRIPT:      getTigressScores.m
%
%  DESCRIPTION: Matlab script to run TIGRESS (matlab code available at 
%               http://cbio.ensmp.fr/~ahaury/svn/dream5/html/index.html).
%               Returns an output file ('tigress_scores.txt') with the scores 
%               matrix of size pxp (p is the total number of ChIP-seq samples).
%				
%  AUTHOR:      Felicia Ng (fn231@cam.ac.uk)
%  INST: 	  	Cambridge Institute for Medical Research
%  VERSION:     1.0
%  CREATED:     22 Oct 2015
%
%%===============================================================================

% Read in data
mydata=read_data('', 'TF_binding_profile');

% Run TIGRESS
freq=tigress(mydata, 'R',500);

% Calculate normalized scores and make matrix symmetric
scores=score_edges(freq,'method','area','L',5);
scoressym=(triu(transpose(scores),1) + triu(scores,1)) / 2
scores2=mat2dataset(scoressym,'VarNames',mydata.genenames,'ObsNames',mydata.genenames)

% Write scores to file
export(scores2,'file','tigress_scores.txt','Delimiter','\t','WriteVarNames',true,'WriteObsNames',true)