#  DESCRIPTION: Various functions called by network creation scripts 
#				
#  AUTHOR:      Felicia Ng (fn231@cam.ac.uk)
#  INST: 	  	Cambridge Institute for Medical Research
#  VERSION:     1.0
#  CREATED:     22 Oct 2015

##===============================================================================
#
#  FUNCTION:    getUniqueEdges
#
#  DESCRIPTION: Function to obtain unique edges such that only undirected edges 
#				(A-B is equivalent to B-A) and non-self regulating edges are 
#				considered.  
#				
#  ARGUMENTS:
#  edgesmat:   
#    Data matrix of edges. Column 1 and 2 are sample names.
#  labels:
#	 List of all sample names
#
#  RETURN:  	Square matrix of binary values where '1' indicates unique edges
#
##==============================================================================

getUniqueEdges <- function(edgesmat, labels) {
	
	# Tabulate edges
	node1 <- factor(edgesmat[,1], levels = labels)
	node2 <- factor(edgesmat[,2], levels = labels)
	edges.tbl <- table(data.frame(node1, node2))
	
	# Treat all edges as undirected: A-B = B-A
	edges.merge <- matrix(ncol=length(labels), nrow=length(labels)) 
	edges.merge[upper.tri(edges.merge)] <- edges.tbl[upper.tri(edges.tbl)] + t(edges.tbl)[upper.tri(edges.tbl)]
	edges.uniq <- apply(edges.merge, 2, function(x) replace(x, x>1, 1))
	
	rownames(edges.uniq) <- labels
	colnames(edges.uniq) <- labels
	
	return(edges.uniq)
	
}

##===============================================================================
#
#  FUNCTION:    getConsensusEdges
#
#  DESCRIPTION: Function to extract edges discovered by multiple algorithms.
#				
#  ARGUMENTS:
#  count:   
#    Square matrix of edges count
#  thr:
#	 Minimum threshold for consensus edges
#
#  RETURN:  	returns a matrix of 3 columns - node1, interaction, and node2
#
##==============================================================================

getConsensusEdges <- function(count, thr) {
	
	consensus.edges.idx <- which(count>=consensusthr, arr.ind=T)
	consensus.node1 <- rownames(count)[consensus.edges.idx[,1]]
	consensus.node2 <- colnames(count)[consensus.edges.idx[,2]]
	consensus <- data.frame(node1=consensus.node1, interaction=rep("pp", nrow(consensus.edges.idx)), node2=consensus.node2)
	
	return(consensus)
}