# README #

This repository contains R and matlab source code as well as data to reproduce the 'TF association network' described in in Ng et al. To do this, follow the instructions below using the data provided in the 'data' folder. The code may also be used to infer a network from a new collection of ChIP-seq experiments.

### Constructing a TF association network ###

A TF association network is a graph representation of the likely causal relationships between multiple ChIP-seq experiments. Using the TF global binding profiles as starting points, the code provided here will construct a network with undirected edges for all pairs of TFs whose binding are directly dependent on each other. The notion of conditional independence in graphical modeling theory is used to distinguish direct vs indirect interactions between TFs.

Four algorithms used to infer the network in Ng et al. are:

* Bayesian network (bnlearn) (Scutari et al., 2010)
* Gaussian Graphical Model (GeneNet) (Schafer et al., 2005)
* Graphical Lasso (glasso) (Friedman et al., 2008)
* LARS with stability selection (TIGRESS) (Haury et al., 2012). 

### Setting up ###

1. Clone/download the repository contents, which include:
    * R and matlab source code
    * input data or TF binding profile of multiple ChIP-seq experiments with the following format (one column per experiment):

            chr   start  end  sample1  sample2  sample3  sample4
            chr1  10     20	  1        1	    1	     1
            chr1  30     40	  1        1	    0	     1
            chr1  50     60	  0        0	    1	     1

2. Install the following R packages:
    * [ _bnlearn_ ](https://cran.r-project.org/web/packages/bnlearn/ "bnlearn")
    * [ _GeneNet_ ](https://cran.r-project.org/web/packages/GeneNet/index.html "GeneNet")
    * [ _glasso_ ](https://cran.r-project.org/web/packages/glasso/index.html "glasso")
    * [ _ggplot2_ ](https://cran.r-project.org/web/packages/ggplot2/index.html "ggplot2")
    * [ _igraph_ ](https://cran.r-project.org/web/packages/igraph/index.html "igraph")

3. Download the [ _TIGRESS_ ](http://cbio.ensmp.fr/~ahaury/svn/dream5/html/index.html "TIGRESS") matlab code:

#### Software versions used in the study: ####
* _R_ version 3.1.2
* _bnlearn_ version 3.7.1
* _GeneNet_ version 1.2.12
* _glasso_ version 1.8
* _ggplot2_ version 1.0.0
* _igraph_ version 0.7.1
* _MATLAB_ version 8.3
* _TIGRESS_ version 2.1

### Using the code ###

First, Steps 1-4 generates four individual lists of network edges using the four inference algorithms. In each step, the code reads in the input data file "TF binding profile.txt" and calculate a matrix (of size pxp for p samples) of conditional dependence measure. A threshold is then applied to the matrix to determine the set of 'direct' interaction edges between TFs. To determine a suitable level of network sparsity, a set of different thresholds are used and the output from Steps 1-4 are multiple list of edges that pass the different thresholds. Finally, Step 5 generates a consensus network by identifying edges that were discovered as 'direct' interactions in multiple algorithms.

***Step 1*** Bayesian network
```
#!R
    
	> source("runBnlearn.R")
```
***Step 2*** Gaussian Graphical Model
```
#!R

	> source("runGeneNet.R")
```
***Step 3*** Graphical Lasso
```
#!R

	> source("runGlasso.R")
```
***Step 4*** LARS with stability selection (TIGRESS)

In matlab, calculate a score matrix using the TIGRESS software:
```
#!matlab

	>> run getTigressScores.m
```
Then, in R identify 'direct' edges:
```
#!R

	> source("runTigress.R")
```
***Step 5*** Obtain consensus network.

```
#!R

	> source("getConsensusEdges.R")

```

The output from the final step is a '.sif' format file containing consensus edges which can be imported into Cytoscape for visualization.

### Contact details ###

Felicia Ng (fn231 [at] cam.ac.uk)

### References ###

* Scutari M (2010) _Learning Bayesian Networks with the bnlearn R Package._ Journal of Statistical Software 35(3):1-22. [link](http://www.jstatsoft.org/article/view/v035i03)
* Schafer J & Strimmer K (2005) _A shrinkage approach to large-scale covariance matrix estimation and implications for functional genomics._ Stat Appl Genet Mol Biol 4:Article32. [pubmed](http://www.ncbi.nlm.nih.gov/pubmed/16646851)
* Friedman J, Hastie T, & Tibshirani R (2008) _Sparse inverse covariance estimation with the graphical lasso._ Biostatistics 9(3):432-441. [pubmed](http://www.ncbi.nlm.nih.gov/pubmed/18079126)
* Haury AC, Mordelet F, Vera-Licona P, & Vert JP (2012) _TIGRESS: Trustful Inference of Gene REgulation using Stability Selection._ BMC Syst Biol 6:145. [pubmed](http://www.ncbi.nlm.nih.gov/pubmed/23173819)